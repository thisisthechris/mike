'use strict';

angular.module('webApp')
    .factory('socket', function (socketFactory) {
        var socket = socketFactory({
            ioSocket: io.connect('/back')
        });
        socket.forward('error');
        return socket;
    });
