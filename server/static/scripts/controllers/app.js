'use strict';

angular.module('webApp')
    .controller('Frontend', function ($scope, socket) {
        //notifications manage
       

        toastr.options = {
          "closeButton": false,
          "debug": false,
          "newestOnTop": false,
          "progressBar": false,
          "positionClass": "toast-bottom-left",
          "preventDuplicates": false,
          "onclick": null,
          "showDuration": "300",
          "hideDuration": "1000",
          "timeOut": "5000",
          "extendedTimeOut": "1000",
          "showEasing": "swing",
          "hideEasing": "linear",
          "showMethod": "fadeIn",
          "hideMethod": "fadeOut"
        }

        //messages comming from the server
        //make them alerts
        socket.forward('notification', $scope);
        $scope.$on('socket:notification', function (ev, data) {
            console.log(data);
            toastr.info(data);
            //we can put sync here to change all together
        });

        socket.forward('tweet', $scope);
        socket.forward('similar_words_list', $scope);
        socket.forward('odd_words_list', $scope);
        $scope.tweet_responce = "";
        $scope.word_responce = "";

        $scope.$on('socket:tweet', function (ev, data) {
            $scope.tweet_responce = data.msg;
        });
        $scope.$on('socket:similar_words_list', function (ev, data) {
            console.log(data);
            $scope.similar_words_list_responce = data.msg; 

        });
         $scope.$on('socket:odd_words_list', function (ev, data) {
            console.log(data);
            $scope.odd_word_responce = data.msg; 

        });
        $scope.tweet = "";
        $scope.word = "";
        $scope.tweet_submit = function(){
            if ($scope.tweet != ""){
                socket.emit('tweet',{
                        tweet: $scope.tweet
                    });
                console.log("emiting tweet", $scope.tweet);
                $scope.tweet = "";
            }

        }
        $scope.word_submit = function(){
            if ($scope.word != ""){
                socket.emit('word',{
                        word: $scope.word
                    });
                console.log("emiting word", $scope.word);
                $scope.word = "";
            }
        }
        $scope.odd_word_submit = function(){
            if ($scope.odd_word_list != ""){
                socket.emit('odd_word_list',{
                        words: $scope.odd_word_list
                    });
                console.log("emiting word", $scope.odd_word_list);
                $scope.odd_word_list = "";
            }
        }
       


    });
