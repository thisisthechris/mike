import numpy as np
import theano
import theano.tensor as TT
import sys, os, pickle
from collections import OrderedDict


class RNN_BPTT():
    def __init__(self, nh, ni, no):


        # number of hidden units
        self.n = nh
        # number of input units
        self.nin = ni
        # number of output units
        self.nout = no

        # input (where first dimension is time)
        self.u = TT.matrix()
        # target (where first dimension is time)
        self.t = TT.matrix()
        # initial hidden state of the RNN
        self.h0  = theano.shared(np.zeros(nh, dtype=theano.config.floatX))
        # learning rate
        self.lr = TT.scalar()
        # momentum rate
        self.mom = TT.scalar()

        self.index = TT.lscalar()



        scale = 0.2
        #output bias
        self.outb = theano.shared(np.ones((1)).astype(theano.config.floatX))
        # recurrent weights as a shared variable
        self.W = theano.shared(scale*np.random.uniform(size=(self.n, self.n), low=-1, high=1).astype(theano.config.floatX))
        # input to hidden layer weights
        self.W_in = theano.shared(scale*np.random.uniform(size=(self.nin, self.n), low=-1, high=1).astype(theano.config.floatX))
        # hidden to output layer weights
        self.W_out = theano.shared(scale*np.random.uniform(size=(self.n, self.nout), low=-1, high=1).astype(theano.config.floatX))
        # hidden to output bias weights
        self.W_outb = theano.shared(scale*np.random.uniform(size=(1, self.nout), low=-1, high=1).astype(theano.config.floatX))

        self.params = [ self.W, self.W_in, self.W_out, self.W_outb]
    def _inner_step(self,):

        # recurrent function (using tanh activation function) and linear output
        # activation function
        def step(u_t, h_tm1, W, W_in, W_out, W_outb, outb):
            h_t = TT.nnet.sigmoid(TT.dot(u_t, W_in)+  TT.dot(h_tm1, W))
            y_t = TT.nnet.sigmoid(TT.dot(h_t, W_out) + TT.dot(outb, W_outb))
            return h_t, y_t

        # the hidden state `h` for the entire sequence, and the output for the
        # entrie sequence `y` (first dimension is always time)
        [h, y], _ = theano.scan(step,
                                sequences=[self.u],
                                outputs_info=[self.h0, None],
                                non_sequences=[self.W, self.W_in, self.W_out, self.W_outb, self.outb])
        return h,y

    def _net_trainer_accumulator(self,):

        h,y = self._inner_step()

        # error between output and target
        error = ((y - self.t) ** 2).sum()

        # gradients on the weights using BPTT
        # gW, gW_in, gW_out, gW_outb = TT.grad(error, [self.W, self.W_in, self.W_out, self.W_outb])
        gradients = TT.grad(error, self.params )
        updates = OrderedDict(( p, p-self.lr*g ) for p, g in zip( self.params , gradients))
        # training function, that computes the error and updates the weights using
        # SGD.

        return theano.function([self.u, self.t, self.lr],
                        [error,y],
                        updates=updates
                        )
        # {(self.W, self.W - self.lr * gW),
        # (self.W_in, self.W_in - self.lr * gW_in),
        # (self.W_out, self.W_out - self.lr * gW_out),
        # (self.W_outb, self.W_outb - self.lr * gW_outb)}

    def _net_step(self,):
        # make the inner step
        h,y = self._inner_step()

        # compile and return the function
        return theano.function(
            inputs=[self.u],outputs=[ h, y]
            )

    def _load_weights(self,filename):
        import pickle
        folder = "trained_elman_pb/"

        f = open(folder+filename+".pickle","rb")

        net_w = pickle.load(f)

        self.W.set_value(
            np.asarray(net_w[0].get_value()))
        self.W_in.set_value(
            np.asarray(net_w[1].get_value()))
        self.W_pb.set_value(
            np.asarray(net_w[2].get_value()))
        self.W_out.set_value(
            np.asarray(net_w[3].get_value()))
        self.W_outb.set_value(
            np.asarray(net_w[4].get_value()))

        f.close()

    def _save(self, folder):
        for i,param in enumerate(self.params):
            np.save(os.path.join(folder, str(i)+ '.npy'), param.get_value())

    def _load(self, folder):
        for i,param in enumerate(self.params):
            param.set_value(np.load(os.path.join(folder, str(i)+ '.npy')))
