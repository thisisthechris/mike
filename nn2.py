import numpy as np
import gensim
from tools import shuffle, minibatch, contextwin, loadFile
from elman2 import RNN_BPTT as rnn
import theano

import pdb


def train():
    global network
    word2vec_model = gensim.models.Word2Vec.load("skipgram_all_data")

    raw_dataset = loadFile("all_data_saved.pickle")

    sentences = []
    for sentence in raw_dataset:
        new_sentence = []
        for word in sentence:
            new_sentence.append(word2vec_model[word[0]])
        sentences.append(new_sentence)


    #embedding dimension
    emb_dimension = word2vec_model["one"].shape[0]

    #net parameters
    n_hp = {
        "in_d":emb_dimension,
        "h_d":emb_dimension*3,
        "out_d":emb_dimension,
        "lr":0.0627142536696559,
        "epochs":100,
        "seed":345,
        "bs":5,
        "c_win":7
        }
    #init the network
    network = rnn(
        n_hp["h_d"],
        n_hp["in_d"],
        n_hp["out_d"]
        )
    trainer = network._net_trainer_accumulator()

    stepper = network._net_step()

    for i in xrange(len(sentences)):
        #prepare the minibatch
        words  = map(lambda x: np.asarray(x).astype(theano.config.floatX),\
                  minibatch(sentences[i], n_hp['bs']))

        #slide for prediction
        target = sentences[i][1:]
        #append the end of the sentence
        target.append(np.zeros(n_hp['in_d']))
        #prepare the target
        targets = map(lambda x: np.asarray(x).astype(theano.config.floatX),\
                  minibatch(target, n_hp['bs']))



    for e in xrange(n_hp["epochs"]):

        shuffle(sentences, n_hp['seed'])
        GE = .0
        for i in xrange(len(sentences)):

            Error = 0.
            for inp,outp in zip (words, targets):
                # import pdb; pdb.set_trace()
                error, output = trainer(inp,outp,n_hp["lr"])
                Error += error

            print "Sentence ", i, " error ", Error/n_hp['bs']
            print "The sentence is :"
            # for word in output:
            #     out = word2vec_model.most_similar(positive=[word],topn=1)
            #     print " ",out[0][0],
            print " "
            GE +=Error
            if i%100==0:
                network._save("nn2_save")
        print "Epoch ",e," error ", GE
        #import pdb; pdb.set_trace()
        network._save("nn2_save")

def run():
    global network
    word2vec_model = gensim.models.Word2Vec.load("skipgram_all_data")

    raw_dataset = loadFile("all_data_saved.pickle")

    sentences = []
    for sentence in raw_dataset:
        new_sentence = []
        for word in sentence:
            new_sentence.append(word2vec_model[word[0]])
        sentences.append(new_sentence)


    #embedding dimension
    emb_dimension = word2vec_model["one"].shape[0]

    #net parameters
    n_hp = {
        "in_d":emb_dimension,
        "h_d":emb_dimension*3,
        "out_d":emb_dimension,
        "lr":0.00627142536696559,
        "epochs":100,
        "seed":345,
        "bs":9,
        "c_win":7
        }
    #init the network
    network = rnn(
        n_hp["h_d"],
        n_hp["in_d"],
        n_hp["out_d"]
        )
    network._load("trained/nn2_save")

    trainer = network._net_trainer_accumulator()

    stepper = network._net_step()

    def step(words):
        go = True
        rep_counter = 0
        while go:
            vector = []
            for word in words:
                #find its vector
                vector.append(word2vec_model[word])
            #put it in the network
            inputs  = map(lambda x: np.asarray(x).astype(theano.config.floatX),\
                  minibatch(vector, n_hp['bs']))
            for inp in inputs:
                h, y = stepper(inp)
            #take output
            word_out = word2vec_model.most_similar(positive=[y],topn=1)[0][0]
            if word_out not in words:
                words.append(word_out)
                rep_counter +=1
            mse = np.sum((y)**2)/y.shape[0]
            if len(words)>20 or (mse<0.0020) or (rep_counter>5):
                return words, [h,y]

    word_out, [h,y] = step("rediscover london town".split(" "))
    print word_out
    import pdb;pdb.set_trace()
if __name__=="__main__":
    # try:
    run()
    # except:
        #import pdb; pdb.set_trace()
        # network._save("nn2_save")
