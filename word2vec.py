import gensim
import pickle
from tools import loadFile


if __name__=="__main__":

    #load the dataset
    dataset = loadFile("all_data_saved.pickle")

    #make it gensim capable
    new_dataset = []
    for sentence in dataset:
        new_sentence = []
        for word in sentence:
            new_sentence.append(word[0])
        new_dataset.append(new_sentence)

    #make the gensim model
    #c-bow training
    model1 = gensim.models.Word2Vec(
        new_dataset, workers=16, sg=2, iter=30, min_count=1
        )
    #skip-gram train
    model2 = gensim.models.Word2Vec(
            new_dataset, workers=16, sg=1, iter=30, min_count=1
            )

    model1.save("cbow_all_data_2")
    model2.save("skipgram_all_data_2")
    # import pdb; pdb.set_trace()
